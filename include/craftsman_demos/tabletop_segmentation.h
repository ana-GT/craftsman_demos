/**
 * @file tabletop_segmentation.h
 */
#pragma once

#include <string>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/segmentation/extract_clusters.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>


/**
 * @class TabletopSegmentor
 */
template<typename PointT>
class TabletopSegmentor {
      
public:

    typedef pcl::PointCloud<PointT> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
    typedef typename pcl::search::KdTree<PointT>::Ptr KdTreePtr;


    /** Constructor */
    TabletopSegmentor(); 
    
    /** Destructor */
    ~TabletopSegmentor() {}
    
    /** Set filter parameters */
    void set_filter_minMax( const double &_xmin, const double &_xmax,
			    const double &_ymin, const double &_ymax,
			    const double &_zmin, const double &_zmax );
    void set_table_object_filter( double _table_obj_height_filter_min,
				  double _table_obj_height_filter_max );
    void set_min_cluster_size( int _size ) { min_cluster_size_ = _size; }
    void reset_table_coeffs() { mTableCoeffs.resize(0); }
    void set_table_coeffs( double _t0, double _t1, double _t2, double _t3 ) {
      mTableCoeffs.resize(4);
      mTableCoeffs[0] = _t0;
      mTableCoeffs[1] = _t1;
      mTableCoeffs[2] = _t2;
      mTableCoeffs[3] = _t3;
    }
    //------------------- Complete processing -----
    
    //! Complete processing for new style point cloud
    bool processCloud( const PointCloudConstPtr &_cloud, // PointCloudConstPtr
		       bool _table_coeffs_calculate = true,
		       bool _debug = false );
    bool filter_pointcloud( const PointCloudConstPtr &_cloud, // ConstPtr
			    PointCloudPtr &_cloud_filtered_ptr );
    bool get_table( PointCloudPtr _cloud_ptr,
		    bool _debug );
    // Only call after get_image_on_top
    PointCloud get_top_points() { return mTop_Points; }
    
    bool get_objects_on_top( const PointCloudConstPtr &_cloud, // ConstPtr
			     PointCloudPtr &_cloud_objects_ptr );
    cv::Mat get_image_on_top( const PointCloudConstPtr &_cloud,
			      int _width,
			      int _height,
			      unsigned char _below_plane = 0,
			      unsigned char _shadow = 125,
			      unsigned char _above_plane = 255 );
    int getNumClusters() { return mClusters.size(); }
    PointCloud getCluster( int _ind ) { 
      return mClusters[_ind];	  
    }
    pcl::PointIndices getClusterIndices( int _ind ) {
      return mClusterInds[_ind];
    }

    PointCloud getTable() { return mTable_Points; }
    Eigen::Isometry3d getTableTf() { return mTableTf; }
    std::vector<double> getTableCoeffs() { return mTableCoeffs; }
    void set_up_direction_z( double _up_direction ) { up_direction_  = _up_direction; }
    
    pcl::ConvexHull<pcl::PointXYZ> ph;
 private:
    
    //! Min number of inliers for reliable plane detection
    int table_inlier_threshold_;
    //! Size of downsampling grid before performing plane detection
    double plane_detection_voxel_size_;
    //! Size of downsampling grid before performing clustering
    double clustering_voxel_size_;
    //! Filtering of original point cloud along the z, y, and x axes
    double z_filter_min_, z_filter_max_;
    double y_filter_min_, y_filter_max_;
    double x_filter_min_, x_filter_max_;
    //! Filtering of point cloud in table frame after table detection
    double table_obj_height_filter_min_, table_obj_height_filter_max_;
    //! Min distance between two clusters
    double cluster_distance_;
    //! Min number of points for a cluster
    int min_cluster_size_;
    //! Clouds are transformed into this frame before processing; leave empty if clouds
    //! are to be processed in their original frame
    std::string processing_frame_;
    //! Positive or negative z is closer to the "up" direction in the processing frame?
    double up_direction_;
    //! How much the table gets padded in the horizontal direction
    double table_padding_;
    
    /** Info that will be messages */
    std::vector<PointCloud, Eigen::aligned_allocator<PointCloud> > mClusters;
    std::vector<pcl::PointIndices, Eigen::aligned_allocator<pcl::PointIndices> > mClusterInds;
    PointCloud mTable_Points;
    Eigen::Isometry3d mTableTf;
    PointCloud mTableHull_Points;
    std::vector<double> mTableCoeffs;
    PointCloud mTop_Points;
    
 public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
   
};

#include <craftsman_demos/impl/tabletop_segmentation.hpp>
