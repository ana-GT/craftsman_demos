#pragma once

#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>
#include <navigation_interface/RequestPath.h>
#include <trac_fsm_base/state_base.h>
#include <tf/transform_listener.h>

#include <actionlib/client/simple_action_client.h>
#include <navigation_interface/NavigationExecutionAction.h>
#include <affordance_template_client/client.h>
#include <craftsman_demos/LocalizePrimitives.h>

using namespace trac_fsm_base;

/********************************/
namespace craftsman_demos_fsm_states {

/**
 * @class NavigationState
 */
class NavigationState : public trac_fsm_base::StateBase
{
  std::string path_frame_;
  std::string robot_frame_;
  bool running_;
  
  ros::ServiceClient path_request_;  
  std::shared_ptr<actionlib::SimpleActionClient<navigation_interface::NavigationExecutionAction> > path_execute_client_;
  tf::TransformListener listener_;

  geometry_msgs::Pose goal_pose_;
public:
  NavigationState(){}
  
  bool configure();
  StateReturn execute();
};

/**
 * @class LocalizationState
 */
class LocalizationState : public trac_fsm_base::StateBase
{
  std::string template_;
  std::string object_;
  
  ros::ServiceClient localize_request_;    
  tf::TransformListener listener_;

  //geometry_msgs::Pose goal_pose_;
  affordance_template_client::AffordanceTemplateClient* client_ = affordance_template_client::AffordanceTemplateClient::getInstance(nh_);

public:
  LocalizationState(){}
  
  bool configure();
  StateReturn execute();
};



}; // craftsman_demos_fsm_states
