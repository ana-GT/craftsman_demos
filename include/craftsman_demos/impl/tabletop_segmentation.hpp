/**
* @file tabletop_segmentation.hpp
*/
#pragma once
/**
* @file tabletop_segmentation.hpp
* @brief Based on code from Marius Muja and Matei Ciocarlie
*/
#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/surface/convex_hull.h>
#include <ros/ros.h>


// NOTES
// READ WHAT RADU SAID!
//http://www.pcl-users.org/Point-Cloud-processing-in-grabber-callback-td3928466.html
/*************** HELPERS ***********************/

/**
 * @function getClustersFromPointCloud2
 */
template <typename PointT>
void getClustersFromPointCloud2( const pcl::PointCloud<PointT> &_cloud_objects,
				 const std::vector<pcl::PointIndices> &_clusters2,
				 std::vector<pcl::PointCloud<PointT> > &_clusters ) {
  // Resize
  _clusters.resize( _clusters2.size() );
  // Fill the cluster with the indices
  for (size_t i = 0; i < _clusters2.size (); ++i) {
    pcl::PointCloud<PointT> cloud_cluster;
    pcl::copyPointCloud( _cloud_objects, _clusters2[i], cloud_cluster );
    _clusters[i] = cloud_cluster;
  }
}

/**
 * @function correct_plane_direction
 */
bool correct_plane_direction( std::vector<double> &_coeffs,
			      double _up_direction ) {

  double a = _coeffs[0];
  double b = _coeffs[1];
  double c = _coeffs[2];
  double d = _coeffs[3];
  //asume plane coefficients are normalized
  Eigen::Vector3d z(a, b, c);
  
  //make sure z points "up" (the plane normal and the table must have a > 90 angle, hence the cosine must be negative)
  if ( z.dot( Eigen::Vector3d(0, 0, _up_direction) ) > 0) {
    _coeffs[0]*= -1; _coeffs[1]*= -1;
    _coeffs[2]*= -1; _coeffs[3]*= -1;
    return true;
  } else {
    return false;
  }

}

/**
 * @function getPlaneTransform
 * @brief Assumes plane coefficients are of the form ax+by+cz+d=0, normalized
*/
Eigen::Matrix4d getPlaneTransform ( pcl::ModelCoefficients &coeffs,
				    double up_direction ) {
  Eigen::Matrix4d tf = Eigen::Matrix4d::Identity();
  if( coeffs.values.size() <= 3 ) {
    std::cout << "[ERROR] Coefficient size less than 3. I will output nonsense values"<<std::endl;
    return tf;
  }

  double a = coeffs.values[0];
  double b = coeffs.values[1];
  double c = coeffs.values[2];
  double d = coeffs.values[3];
  Eigen::Vector3d position(-a*d, -b*d, -c*d);
  Eigen::Vector3d z(a, b, c);

  
  //try to align the x axis with the x axis of the original frame
  //or the y axis if z and x are too close too each other
  Eigen::Vector3d x; x << 1, 0, 0;
  if ( fabs(z.dot(x)) > 1.0 - 1.0e-4) x = Eigen::Vector3d(0, 1, 0);
  Eigen::Vector3d y = z.cross(x); y.normalized();
  x = y.cross(z); x.normalized();
  Eigen::Matrix3d rotation;
  rotation.block(0,0,3,1) = x;
  rotation.block(0,1,3,1) = y;
  rotation.block(0,2,3,1) = z;
  Eigen::Quaterniond orientation( rotation );
  tf.block(0,0,3,3) = orientation.matrix();
  tf.block(0,3,3,1) = position;
  return tf;
}

/*********** CLASS FUNCTIONS ***************/
/**
 * @function TabletopSegmentor
 * @brief Constructor
 */
template<typename PointT>
TabletopSegmentor<PointT>::TabletopSegmentor() {

  table_inlier_threshold_ = 300;
  plane_detection_voxel_size_ = 0.005; // 0.01
  clustering_voxel_size_ = 0.0015; // 1.5mm

  z_filter_min_ = 0.7; z_filter_max_ = 1.10; 
  y_filter_min_ = -1.0;  y_filter_max_ = 1.0;
  x_filter_min_ = -1.0; x_filter_max_ = 1.0;

  table_obj_height_filter_min_= 0.01; // 0.0025
  table_obj_height_filter_max_= 0.10;
  cluster_distance_ = 0.02;
  min_cluster_size_ = 300;
  processing_frame_ = "";
  up_direction_ = -1.0;
  // For Photoneo, our input cloud is from the sensor, so our up is +Z
  // When input cloud is world (with photoneo upside down), up is -1.0
  table_padding_ = 0.0;
}

/**
 * @function set_table_object_filter
 */
template<typename PointT>
void TabletopSegmentor<PointT>::set_table_object_filter( double _table_obj_height_filter_min,
							 double _table_obj_height_filter_max ) {
  table_obj_height_filter_min_= _table_obj_height_filter_min; 
  table_obj_height_filter_max_= _table_obj_height_filter_max;
}

/**
 * @function set_filter_minMax
 */
template<typename PointT>
void TabletopSegmentor<PointT>::set_filter_minMax( const double &_xmin, const double &_xmax,
						   const double &_ymin, const double &_ymax,
						   const double &_zmin, const double &_zmax ) {

  x_filter_min_ = _xmin; x_filter_max_ = _xmax;
  y_filter_min_ = _ymin;  y_filter_max_ = _ymax;
  z_filter_min_ = _zmin; z_filter_max_ = _zmax;  
}

/**
 * @function filter_pointcloud
 */
template<typename PointT>
bool TabletopSegmentor<PointT>::filter_pointcloud( const PointCloudConstPtr &_cloud,
						   PointCloudPtr &_cloud_filtered_ptr ) {

  // Filter in X, Y and Z directions: output= cloud_filtered_ptr
  pcl::PassThrough<PointT> pass;

  pass.setInputCloud (_cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (z_filter_min_, z_filter_max_);
  PointCloudPtr z_cloud_filtered_ptr (new PointCloud);
  pass.filter (*z_cloud_filtered_ptr);
  pass.setInputCloud (z_cloud_filtered_ptr);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (y_filter_min_, y_filter_max_);
  PointCloudPtr y_cloud_filtered_ptr (new PointCloud);
  pass.filter (*y_cloud_filtered_ptr);
  pass.setInputCloud (y_cloud_filtered_ptr);
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (x_filter_min_, x_filter_max_);

  pass.filter (*_cloud_filtered_ptr);
  // Check that the points filtered at least are of a minimum size
  if (_cloud_filtered_ptr->points.size() < (unsigned int)min_cluster_size_) {
    ROS_ERROR("Filtered cloud only has %d points",
              (int)_cloud_filtered_ptr->points.size() );
    return false;
  }

  
  return true;
}

/**
 * @function get_table
 */
template<typename PointT>
bool TabletopSegmentor<PointT>::get_table( PointCloudPtr _cloud_ptr,
					   bool _debug ) {

  pcl::VoxelGrid<PointT> grid_;
  KdTreePtr normals_tree_;
  pcl::NormalEstimation<PointT, pcl::Normal> n3d_;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg_;
  pcl::ProjectInliers<PointT> proj_;

  PointCloudPtr table_hull_ptr (new PointCloud);

  grid_.setLeafSize (plane_detection_voxel_size_,
		     plane_detection_voxel_size_,
		     plane_detection_voxel_size_);  
  grid_.setFilterFieldName ("z");
  grid_.setFilterLimits (z_filter_min_, z_filter_max_);
  grid_.setDownsampleAllData (false);
  
  normals_tree_ = boost::make_shared<pcl::search::KdTree<PointT> > ();  

  // Normal estimation parameters
  n3d_.setKSearch (10);
  n3d_.setSearchMethod (normals_tree_);

  // Table model fitting parameters
  seg_.setDistanceThreshold (0.01);
  seg_.setMaxIterations (1000); // 10000
  seg_.setNormalDistanceWeight (0.1);
  seg_.setOptimizeCoefficients (true);
  seg_.setModelType (pcl::SACMODEL_PLANE);
  
  seg_.setMethodType (pcl::SAC_RANSAC);
  seg_.setProbability (0.99);
  
  proj_.setModelType (pcl::SACMODEL_PERPENDICULAR_PLANE);
  

  
  // Downsample the filtered cloud: output = cloud_downsampled_ptr
  PointCloudPtr cloud_downsampled_ptr (new PointCloud);
  grid_.setInputCloud (_cloud_ptr);
  grid_.filter (*cloud_downsampled_ptr);
  
  if (cloud_downsampled_ptr->points.size() < (unsigned int)min_cluster_size_) {
    ROS_ERROR( "Downsampled cloud only has %d points",
               (int)cloud_downsampled_ptr->points.size() );
    return false;
  }

  ROS_WARN( "Downsampled cloud has %d points / %d.",
            (int)cloud_downsampled_ptr->points.size(),
            (int)_cloud_ptr->points.size() );


  /***************** Step 2 : Estimate normals ******************/
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals_ptr (new pcl::PointCloud<pcl::Normal>);
  n3d_.setInputCloud (cloud_downsampled_ptr);
  n3d_.compute (*cloud_normals_ptr);

  /************* Step 3 : Perform planar segmentation, **********/
  /** if table is not given, otherwise use given table */
  Eigen::Matrix4d table_plane_trans;
  Eigen::Matrix4d table_plane_trans_flat;
  pcl::PointIndices::Ptr table_inliers_ptr (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr table_coefficients_ptr (new pcl::ModelCoefficients);
  seg_.setInputCloud (cloud_downsampled_ptr);
  seg_.setInputNormals (cloud_normals_ptr);
  seg_.segment( *table_inliers_ptr,
		*table_coefficients_ptr );
  // Check the coefficients and inliers are above threshold values
  if (table_coefficients_ptr->values.size () <=3 ) {
    ROS_ERROR( "Failed to detect table in scan" );
    return false;
  }
  if ( table_inliers_ptr->indices.size() < (unsigned int)table_inlier_threshold_) {
    ROS_ERROR( "Plane detection has %d below min thresh of %d points",
               (int)table_inliers_ptr->indices.size(),
               table_inlier_threshold_ );
    return false;
  }

  // Store table's plane coefficients (after calling getPlaneTransform! because here we check if a sign change is needed)
  mTableCoeffs.resize(4);
  for( int i = 0; i < 4; ++i ) { mTableCoeffs[i] = table_coefficients_ptr->values[i]; }


  if( !correct_plane_direction( mTableCoeffs, up_direction_ ) ) {
    ROS_INFO("No correction needed");
  } else {
    ROS_INFO("Correction applied");
  }
  for( int i = 0; i < mTableCoeffs.size(); ++i ) {
    ROS_INFO("Table[%d]: %f", i, mTableCoeffs[i] );
  }

  
  if( _debug ) {
      
    // Save points in the hull with some points down
    mTable_Points.points.resize(0);
    for( int i = 0; i < table_hull_ptr->points.size(); ++i ) {
      mTable_Points.points.push_back(table_hull_ptr->points[i]);
    }
    mTable_Points.width = 1; mTable_Points.height = mTable_Points.points.size();

    
    pcl::ExtractIndices<PointT> extract;
    PointCloudPtr tablePointsPtr( new PointCloud );
    extract.setInputCloud( cloud_downsampled_ptr );
    extract.setIndices( table_inliers_ptr );
    extract.setNegative(false);
    extract.filter( *tablePointsPtr );

    int na = tablePointsPtr->points.size();
    if( na > 0 ) {
      for( int a = 0; a < na; ++a ) {/*
	tablePointsPtr->points[a].r = 255;
	tablePointsPtr->points[a].g = 255;
	tablePointsPtr->points[a].b = 255;
	tablePointsPtr->points[a].a = 255;*/
      }
      pcl::io::savePCDFileASCII( "table_points.pcd", *tablePointsPtr );

    }
  } // if _debug
  
  return true;
}

/**
 * @function get_objects_on_top
 */
  template<typename PointT>
bool TabletopSegmentor<PointT>::get_objects_on_top( const PointCloudConstPtr &_cloud,
						    PointCloudPtr &_cloud_objects_ptr ) {

  for( auto p : _cloud->points ) {

    double d = mTableCoeffs[0]*((p).x) + mTableCoeffs[1]*((p).y) + mTableCoeffs[2]*((p).z) + mTableCoeffs[3];
    if( d > table_obj_height_filter_min_ ) {
      _cloud_objects_ptr->points.push_back( p );
    }    
    
  }
  
  
return true;
}


/**
 * @function get_image_on_top
 */
template<typename PointT>
cv::Mat TabletopSegmentor<PointT>::get_image_on_top( const PointCloudConstPtr &_cloud,
						     int _width,
						     int _height,
						     unsigned char _below_plane,
						     unsigned char _shadow,
						     unsigned char _above_plane ) {
  double d; 
  double x, y, z;
  PointT p;
  ROS_INFO("Cloud size: %d %d size: %d", _cloud->width, _cloud->height, (int)_cloud->points.size() );
  
  cv::Mat result( _height, _width, CV_8UC1 );
  typename pcl::PointCloud<PointT>::const_iterator it;
  mTop_Points.points.resize(0);
  it = _cloud->begin();
  for( unsigned int j = 0; j < _height; ++j ) {
    for( unsigned int i = 0; i < _width; ++i ) {
      p = *it;      
      d = mTableCoeffs[0]*((p).x) + mTableCoeffs[1]*((p).y) + mTableCoeffs[2]*((p).z) + mTableCoeffs[3];
      if( std::isnan(p.x) || std::isnan(p.y) || std::isnan(p.z) ) {
	ROS_INFO("One of those is nan");
      }
      if( d > table_obj_height_filter_min_ && d < table_obj_height_filter_max_ ) {
	result.at<unsigned char>(j,i) = _above_plane;
	mTop_Points.points.push_back( p );
      } else  {
	result.at<unsigned char>(j,i) = _below_plane;
      }
      if( p.z == 0 ) { result.at<unsigned char>(j,i) = _shadow; }
      it++;
    } // for i
  } // for j
  mTop_Points.width = 1;
  mTop_Points.height = mTop_Points.points.size();
  return result;
  
}

/**
 * @function processCloud
 */
template<typename PointT>
bool TabletopSegmentor<PointT>::processCloud( const PointCloudConstPtr &_cloud,
					      bool _table_coeffs_calculate,
					      bool _debug ) {

  // PCL objects
  KdTreePtr clusters_tree_;
  pcl::VoxelGrid<PointT> grid_objects_;
  pcl::EuclideanClusterExtraction<PointT> pcl_cluster_;

  /************ STEP 0: Parameter Settings **************/
  // Filtering parameters
  grid_objects_.setLeafSize (clustering_voxel_size_,
			     clustering_voxel_size_,
			     clustering_voxel_size_);
  grid_objects_.setDownsampleAllData (false); // true

  clusters_tree_ = boost::make_shared<pcl::search::KdTree<PointT> > ();
  
  
  // Clustering parameters
  pcl_cluster_.setClusterTolerance (cluster_distance_);
  pcl_cluster_.setMinClusterSize (min_cluster_size_);
  pcl_cluster_.setSearchMethod (clusters_tree_);

  /******** Step 1 : Filter, remove NaNs and downsample ***********/
  PointCloudPtr cloud_filtered_ptr (new PointCloud);
  if( !filter_pointcloud( _cloud, cloud_filtered_ptr ) ) {
    ROS_ERROR("Error filtering pointcloud. Returning without segmenting ");
    return false;
  }

  // Perform planar segmentation if you haven't done so already
  if( _table_coeffs_calculate ) {
    if( !get_table( cloud_filtered_ptr, _debug ) ) {
      ROS_ERROR("Error getting table! ");
      return false;
    }
  }

  /******* Step 5 : Get the objects on top of the table ******/
  PointCloudPtr cloud_objects_ptr (new PointCloud);
  get_objects_on_top( cloud_filtered_ptr,
		      cloud_objects_ptr );

  // Downsample the points
  PointCloudPtr cloud_objects_downsampled_ptr (new PointCloud);  
  grid_objects_.setInputCloud (cloud_objects_ptr);
  grid_objects_.filter (*cloud_objects_downsampled_ptr);
  
  ROS_INFO("Object clouds from %ld to %ld ", cloud_objects_ptr->points.size(),
	 cloud_objects_downsampled_ptr->points.size() );
  
  // Step 6: Split the objects into Euclidean clusters
  std::vector<pcl::PointIndices> clusters2;
  ROS_INFO("Extract clusters .... ");
  pcl_cluster_.setInputCloud (cloud_objects_downsampled_ptr); 
  pcl_cluster_.extract (clusters2);
  mClusterInds.resize( clusters2.size() );
  for( int i = 0; i < clusters2.size(); ++i ) {
    mClusterInds[i] = clusters2[i];
    ROS_INFO("Cluster %d size: %ld ", i, clusters2[i].indices.size()   );
  }
  ROS_INFO("End extracting clusters... ");
  
  mClusters.resize( clusters2.size() );
  int i = 0;

  int r, g, b, a;
  for( std::vector<pcl::PointIndices>::const_iterator it = 
	 clusters2.begin (); it != clusters2.end (); ++it ) {

    PointCloud cloud_cluster;
    for (std::vector<int>::const_iterator pit = it->indices.begin ();
	 pit != it->indices.end (); ++pit) {
      PointT p = cloud_objects_downsampled_ptr->points[*pit];
      /*p.r = r;
      p.g = g;
      p.b = b;
      p.a = 255;*/
      cloud_cluster.points.push_back (p);     
    }
    cloud_cluster.width = cloud_cluster.points.size ();
    cloud_cluster.height = 1;
    cloud_cluster.is_dense = true;
    
    mClusters[i] = cloud_cluster;
    i++;
  }

  return true;
}

