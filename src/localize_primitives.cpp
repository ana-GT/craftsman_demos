#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <craftsman_demos/LocalizePrimitives.h>
#include <craftsman_demos/tabletop_segmentation.h>
#include <tf/transform_listener.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <craftsman_demos/pointcloud_tools/sq_fitting/SQ_fitter.h>

#include <mutex>

 /**
  * @class
  */
class LocalizePrimitives {

public:
  
  /**
   * @brief Constructor
   */
  LocalizePrimitives( ros::NodeHandle &_nh ) :
    nh_(_nh),
    cloud_(new pcl::PointCloud<pcl::PointXYZRGBA>) {
    
    sub_points_ = nh_.subscribe("/xtion/depth_registered/points", 1,
				&LocalizePrimitives::pointcloud_cb, this );
    srv_localize_ = nh_.advertiseService("localize_primitives",
					 &LocalizePrimitives::localize, this);
    double xmin,xmax,ymin,ymax,zmin,zmax;
    xmin = 0.0; xmax = 2.0;
    ymin = -0.5; ymax = 0.5;
    zmin = 0.5; zmax = 1.3;

    reference_frame_ = "map";

    tts_.set_filter_minMax(xmin, xmax,ymin,ymax,zmin,zmax);
  }
  
  /**
   * @brief Save cloud
   */
  void pointcloud_cb( const sensor_msgs::PointCloud2ConstPtr &_msg ) {

    sensor_msgs::PointCloud pc, pc_search_frame;
    sensor_msgs::PointCloud2 pc2;
    std::string err_msg;
    
    points_mutex_.lock();
    sensor_msgs::convertPointCloud2ToPointCloud(*_msg, pc);

    // transform cloud into search frame    
    try {
      ros::Time tf_time = ros::Time::now();
      while (!tf_.waitForTransform( reference_frame_,
                                    pc.header.frame_id,
                                    tf_time, ros::Duration(2.0),
                                    ros::Duration(0.01), &err_msg) ) {
        ROS_ERROR_STREAM("pointcloud_cb(): No transform from "
                         <<reference_frame_<<" to "<<pc.header.frame_id<<".  Error: "<<err_msg);
        err_msg="";
        tf_time = ros::Time::now();
      }    
      tf_.transformPointCloud(reference_frame_, pc, pc_search_frame);
      
      sensor_msgs::convertPointCloudToPointCloud2(pc_search_frame,pc2);
      pcl::fromROSMsg(pc2, *cloud_);
    } catch (...) {
      ROS_ERROR_STREAM("[pointcloud_cb] problem transforming PC");
    }
    //
    points_mutex_.unlock();
  }
  
  /**
   * @brief localize
   */
  bool localize( craftsman_demos::LocalizePrimitives::Request &_req,
		 craftsman_demos::LocalizePrimitives::Response &_res ) {

    points_mutex_.lock();

    // Filter table
    if( tts_.processCloud( cloud_, true, false ) ) {
      ROS_WARN("Found %d clusters!", tts_.getNumClusters() );
    }
    // Fit primitive
    int N = tts_.getNumClusters();
    int max_idx = -1; int max_size = -1;
    pcl::PointCloud<pcl::PointXYZRGBA> pi;
    double dim[3]; double trans[3]; double rot[3]; double e[2];
    int siz;
    
    for( int i= 0; i < N; ++i ) {
      pi = tts_.getCluster( i );
      printf("Pi size: %d max size: %d \n", pi.points.size(), max_size );
      siz = pi.points.size();
      if( siz > max_size ) {
	printf("pI %d bigger size: %d \n",
	       i, pi.points.size());
	max_size = siz; max_idx = i;
      }
    }
    printf("Max idx: %d size: %d \n", max_idx, max_size );
    if( max_idx == -1 ) { _res.success = false; }
    else { pi = tts_.getCluster(max_idx); }
    points_mutex_.unlock();

    // If found, return success, pose and dimensions, if not, return no success
    if( max_idx != -1 ) {
      ROS_WARN("Cluster to be fitted: %d of size: %d \n", max_idx, max_size );
      fit_SQ( pi, dim, trans, rot, e );
      _res.success = true;
      geometry_msgs::PoseStamped pose;
      pose.pose.position.x = trans[0];
      pose.pose.position.y = trans[1];
      pose.pose.position.z = trans[2];
      pose.pose.orientation.w = 1;
      pose.header.frame_id = reference_frame_;
      _res.pose = pose;
    }
    

    return true;
  }

  /**
   * @function fit_SQ
   */
  void fit_SQ( pcl::PointCloud<pcl::PointXYZRGBA> _cluster,
	       double _dim[3], double _trans[3],
	       double _rot[3], double _e[2] ) {
    
    // Generate a mirror version of the pointcloud
    /*mindGapper<pcl::PointXYZRGBA> mg;
      mg.setTablePlane( mTableCoeffs );
      mg.setFittingParams();
      mg.setDeviceParams();
    */
    
    /*pcl::PointCloud<pcl::PointXYZRGBA>::Ptr completed( new pcl::PointCloud<pcl::PointXYZRGBA>() );
    *completed = _cluster;
    mg.reset();
    mg.complete( completed );
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr p( new pcl::PointCloud<pcl::PointXYZ>() );
    p->points.resize( completed->points.size() );
    for(int j = 0; j < completed->points.size(); ++j ) {
      p->points[j].x = completed->points[j].x;
      p->points[j].y = completed->points[j].y;
      p->points[j].z = completed->points[j].z;
    }
    p->width = 1; p->height = p->points.size();
    */
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster( new pcl::PointCloud<pcl::PointXYZRGBA>() );
    *cluster = _cluster;
    fitter_.setInputCloud( cluster );
    double smax = 0.03; // Scale
    double smin = 0.005;
    int N = 5; // NUMBER scales
    double thresh = 0.01;
    
    if( !fitter_.fit( SQ_FX_RADIAL, smax, smin, N, thresh ) ) {
      printf("Something went wrong \n");
    } 
    SQ_parameters p;
    fitter_.getFinalParams( p );
    for( int i = 0; i < 3; ++i ) { _dim[i] = p.dim[i]; }
    for( int i = 0; i < 3; ++i ) { _trans[i] = p.trans[i]; }
    for( int i = 0; i < 3; ++i ) { _rot[i] = p.rot[i]; }
    for( int i = 0; i < 2; ++i ) { _e[i] = p.e[i]; }
    printf("--> SQ dimensions: %f %f %f \n", _dim[0], _dim[1], _dim[2] );
    printf("--> SQ position: %f %f %f \n", _trans[0], _trans[1], _trans[2] );

    
  
}


  
protected:
  ros::NodeHandle nh_;
  ros::Subscriber sub_points_;
  ros::ServiceServer srv_localize_;
  std::mutex points_mutex_;
  TabletopSegmentor<pcl::PointXYZRGBA> tts_;
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_;
  std::string reference_frame_;
  tf::TransformListener tf_;
  // Fit pointcloud to superquadric
  SQ_fitter< pcl::PointXYZRGBA > fitter_;

  
};

////////////////////////////////////
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "localize_primitives" );
  ros::NodeHandle nh;

  LocalizePrimitives lp( nh );
  ros::Rate rate(30);
  
  while( ros::ok() ) {
    ros::spinOnce();
    rate.sleep();
  }

}
