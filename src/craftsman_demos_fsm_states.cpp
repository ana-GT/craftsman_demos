/**
 * @file states.cpp
 */
#include <craftsman_demos/craftsman_demos_fsm_states.h>
#include <tf/transform_datatypes.h>

using namespace craftsman_demos_fsm_states;

// ===================================
// NavigationState
// ===================================
bool NavigationState::configure()
{
  if(verbose_) ROS_INFO_STREAM("state["<<state_name_<<"]:");
  if(verbose_) ROS_INFO_STREAM("  type: "<<state_type_);


  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/robot_frame", robot_frame_)) {
    ROS_ERROR("[NavigationState::configure] could not find robot_frame for state \'%s\'", robot_frame_.c_str());
    return false;
  }


  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/path_frame", path_frame_)) {
    ROS_WARN("[NavigationState::configure] could not find path frame for state  \'%s\'", path_frame_.c_str());
    return false;
  }

  // Goal
  std::vector<double> xyz;
  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/goal/position", xyz)) {
    ROS_WARN("[NavigationState::configure] could not find allowable position error offset for state  \'%s\'",
	     state_name_.c_str());
    return false;
  }
    
  std::vector<double> rpy;
  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/goal/orientation", rpy)) {
    ROS_WARN("[NavigationState::configure] could not find allowable orientation error for state  \'%s\'",
	     state_name_.c_str());
    return false;
  }
    
  if (xyz.size() != 3 || rpy.size() != 3) {
      ROS_WARN("[NavigationState::configure] allowable error vectors not of size 3 each");
      return false;
  }
    
    if(verbose_) ROS_INFO_STREAM("  goal: ");
    if(verbose_) ROS_INFO_STREAM("    position: " << xyz[0] << ", " << xyz[1] << ", " << xyz[2]);
    if(verbose_) ROS_INFO_STREAM("    orientation: " << rpy[0] << ", " << rpy[1] << ", " << rpy[2]);

    // Goal
    goal_pose_.position.x = xyz[0];
    goal_pose_.position.y = xyz[1];
    goal_pose_.position.z = xyz[2];
    tf::Quaternion qt;
    qt = tf::createQuaternionFromRPY( rpy[0], rpy[1], rpy[2] );
    tf::quaternionTFToMsg( qt, goal_pose_.orientation );

  // Set client
  path_request_ = nh_.serviceClient<navigation_interface::RequestPath>("/navigation_interface_node/get_plan");
  path_execute_client_.reset( new actionlib::SimpleActionClient<navigation_interface::NavigationExecutionAction>("/navigation_interface_node/executor", true) );
  ROS_WARN("[NavigationState::configure] Wait for executor server....");
  if(!path_execute_client_->waitForServer(ros::Duration(5.0)) ) { ROS_ERROR("[NavigationState::configure] Didn't find executor action!"); return false; }

  ROS_WARN("[NavigationState::configure] Got executor server....");
  return true;
}

/**
 * @function ICPLocalizationState::execute
 */
StateReturn NavigationState::execute() {
  ROS_WARN("Start NavigationState::execute...");
  tf::StampedTransform tf_0;
  geometry_msgs::Pose p0;

  // Start 
  try{
    listener_.waitForTransform( path_frame_, robot_frame_, ros::Time(0), ros::Duration(10.0) );
    listener_.lookupTransform( path_frame_, robot_frame_,
			       ros::Time(0), tf_0);
  } catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
    return std::make_pair( true, "failure");
  }    
  tf::poseTFToMsg(tf_0, p0);
  ROS_INFO_STREAM("**  Start Pose: "<< p0 ); 
  
  navigation_interface::RequestPath srv;
  srv.request.output_path_frame = path_frame_;
  srv.request.use_map = false;
  srv.request.attempt_escape = false;
  srv.request.path_waypoints.header.frame_id = path_frame_;
  srv.request.path_waypoints.header.stamp = ros::Time::now();

  srv.request.path_waypoints.poses.push_back(p0);
  srv.request.path_waypoints.poses.push_back(goal_pose_);
  

  ROS_INFO_STREAM("**  Goal Pose: "<< goal_pose_ ); 
  // Plan path
  ROS_WARN("[NavigationState::execute] Planning path");
  if ( !path_request_.call(srv) ) {
    ROS_ERROR("****************** NavigationState::execute() -- failed to successfully call planning for part");
    publishFault("Failed to call navigate path service");
    return std::make_pair(true, "failure");
  }
  if( srv.response.paths.size() == 0 ) {
    ROS_ERROR("NavigationState::execute() didn't find a path");
    return std::make_pair(true, "failure");
  }
  
  
  ROS_WARN("[NavigationState::execute] Execute path");
  // Execute path
  navigation_interface::NavigationExecutionGoal goal;
  goal.path = srv.response.paths[0]; 
  goal.direction = srv.response.direction;
  goal.attempt_escape = false;
  path_execute_client_->sendGoal( goal );
  double execution_max_time_ = 20.0;
  bool end_timeout = path_execute_client_->waitForResult( ros::Duration(execution_max_time_) );
  if( end_timeout ) {
    actionlib::SimpleClientGoalState state = path_execute_client_->getState();
    ROS_INFO("\t ** YAY! Path Execute Action finished: %s",state.toString().c_str());
    return std::make_pair(true, "success");
 
  } else {
    ROS_INFO("\t ** Navigation did not end on time %f seconds. Cancelling it", execution_max_time_ );
    path_execute_client_->cancelAllGoals();
    return std::make_pair(true, "failure");
  }
}


// ===================================
// LocalizationState
// ===================================
bool LocalizationState::configure()
{
  if(verbose_) ROS_INFO_STREAM("state["<<state_name_<<"]:");
  if(verbose_) ROS_INFO_STREAM("  type: "<<state_type_);


  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/template", template_)) {
    ROS_ERROR("[LocalizationState::configure] could not find template for state \'%s\'", template_.c_str());
    return false;
  }


  if (!nh_.getParamCached(namespace_ + "/states/" + state_name_ + "/params/object", object_)) {
    ROS_WARN("[LocalizationState::configure] could not find object for state  \'%s\'", object_.c_str());
    return false;
  }
  localize_request_ = nh_.serviceClient<craftsman_demos::LocalizePrimitives>("/localize_primitives");
  
  //ROS_WARN("[LocalizationState::configure] Got executor server....");
  return true;
}

/**
 * @function LocalizationState::execute
 */
StateReturn LocalizationState::execute() {
  ROS_WARN("Start LocalizationState::execute...");


  craftsman_demos::LocalizePrimitives srv;
  
  // Localize
  geometry_msgs::PoseStamped location;
  ROS_WARN("[LocalizeState::execute] Localize");
  if ( !localize_request_.call(srv) ) {
    ROS_ERROR("****************** LocalizeState::execute() -- failed to successfully localize");
    publishFault("Failed to call localize service");
    return std::make_pair(true, "failure");
  }
  if( !srv.response.success ) {
    ROS_ERROR("LocalizationState::execute() didn't find a SQ");
    return std::make_pair(true, "failure");
  } else {
    ROS_WARN("Localized went well!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }

  location = srv.response.pose;
  /*location.header.frame_id = "map";
  location.pose.position.x = 1.615;
  location.pose.position.y = 0.0;
  location.pose.position.z = 0.931;
  */
  if (client_->setObjectPose(template_, object_, location )) {
    ROS_WARN_STREAM("[LocalizationState::execute] "<< state_name_ << " set position to : " << object_);
    return std::make_pair(true, "success");
  } else {
    publishFault("Failed to set pose for object: " + object_);
    ROS_ERROR("[LocalizationState::execute] failed to set pose for client in %s", state_name_.c_str());
    return std::make_pair(true, "failure");
  }

}

//////////////////////////////////////////
PLUGINLIB_EXPORT_CLASS(craftsman_demos_fsm_states::NavigationState, trac_fsm_base::StateBase);
PLUGINLIB_EXPORT_CLASS(craftsman_demos_fsm_states::LocalizationState, trac_fsm_base::StateBase);

